import http from "@/utils/httpTool";

export const requestShopCar = () => http.post('/p/shopCart/info');

// 获取购物车件数
export const requestGetShopCarCount = () => http.get('/p/shopCart/prodCount');

// 计算价格 data是数组，选中的商品id
// 初始进入购物车的时候 checkbox选中的时候 商品数量改变的时候
export const requestReducePrice = (data) => http.post('/p/shopCart/totalPay', data);

// 商品数量改变接口 
// 商品数量发生改变 重新计算商品价格和购物车件数
/**
 * count: 1
prodId: 68
shopId: 1
skuId: 268
 *
 */
export const requestChangeItem  = (data) => http.post('/p/shopCart/changeItem', data);