import http from "@/utils/httpTool";


export const login = (data) => http.post('/login', data);

export const setUserInfo = (data) => http.put('/p/user/setUserInfo', data);