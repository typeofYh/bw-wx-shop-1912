import Vue from 'vue'
import App from './App'
import ShopItem from "@/components/shopItem"
import toPage from "./utils/toPage"
import "@/utils/changeShopIconNumber"
Vue.config.productionTip = false

App.mpType = 'app'
Vue.prototype.$toPage = toPage;
Vue.component('shop-item',ShopItem);

// 新建实例
async function createCloud(){
  const o_cloud = new wx.cloud.Cloud({
    resourceEnv: 'min-gucof', // 环境
    traceUser: true,
  })
  await o_cloud.init() // 初始化
  return o_cloud;
}
Vue.prototype.$cloud = createCloud();

const app = new Vue({
  ...App
})
app.$mount()






