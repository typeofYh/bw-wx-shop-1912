import Vue from "vue";
import { formatUrlQueryString } from "./toPage"
const httpTool = ({timeout, baseUrl, errorHandler, requestHandler}) => {
  const request = async (url, method = 'get', data = {}, header = {}) => {
    const requstObject = {
      originUrl: url,
      baseUrl,
      url: `${baseUrl}${url}`,
      method,
      data,
      header,
      timeout,
    }; 
    const config = requestHandler && requestHandler(requstObject) || requstObject;

    if(typeof config === 'object' && config.url && config.method){ // 请求对象是合法对对象
      try {
        const res = await uni.request(config);
        if(Array.isArray(res) && res.length && res[1]){
          return res[1].data ? res[1].data : res[1];
        }
        return Promise.reject(res);
      }catch(error){
        errorHandler && errorHandler(error);
        return Promise.reject(error);
      }
    }
  }
  return {
    post(url, data, header = {}){
      return request(url, 'POST', data, header)
    },
    get(url, query = {}, header = {}){
      url = formatUrlQueryString(url, query);
      return request(url, 'GET', {}, header)
    },
    put(url, data, header = {}){
      return request(url, 'PUT', data, header)
    }
  }
}

const excludeApi = ['/login'];

const http = httpTool({
  timeout: 10000,
  baseUrl: 'https://bjwz.bwie.com/mall4j',
  requestHandler: (config) => { // 请求前拦截
    const headers = {
      'Authorization': 'bearer' + uni.getStorageSync('token')
    };
    if(excludeApi.includes(config.originUrl)){
      delete headers.Authorization;
    }
    return {
      // 添加公共headers
      ...config,
      header: headers
    }
  },
  errorHandler: (error) => { // 错误拦截函数

  }
});

export default http;

Vue.prototype.$http = http;