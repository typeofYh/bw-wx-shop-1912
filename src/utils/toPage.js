export const formatUrlQueryString = (baseUrl, query) => {
  return Object.keys(query).length ? (baseUrl + '?' + Object.keys(query).map(key => `${key}=${query[key]}`).join('&')) : baseUrl;
}

export default function({url = '', query = {}, redirect = false, tabbar = false}){
  // 处理query拼接url
  url = formatUrlQueryString(url, query);
  const methodName = tabbar ? 'switchTab' : redirect ? 'redirectTo' : 'navigateTo';
  // 如果跳转的页面是tabbar页面使用switchTab跳转
  // 如果redirect值为true使用redirectTo跳转
  // 以上两个条件都不符合使用navigateTo跳转并且解决十次的问题
  return new Promise((resolve, reject) => {
    uni[methodName]({
      url,
      success(res){
        resolve(res);
      },
      fail(error){
        reject(error)
      }
    })
  })
}