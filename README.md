## 页面组成

1. wxml 结构
2. wxss 样式
3. json 配置
4. js   行为

app.js 是小程序的入口文件 (调用App方法实例小程序) （https://developers.weixin.qq.com/miniprogram/dev/reference/api/App.html）
app.json 是小程序主配置文件（https://developers.weixin.qq.com/miniprogram/dev/reference/configuration/app.html#sitemapLocation）
app.wxss 全局样式
project.config.json 项目配置文件 (https://developers.weixin.qq.com/miniprogram/dev/devtools/projectconfig.html)
sitemap.json 小程序描述文件，小程序搜索的关键词设置 (https://developers.weixin.qq.com/miniprogram/dev/reference/configuration/sitemap.html)

## App函数
小程序应用生命周期
1. onLaunch
2. onShow
3. onHide
4. onError

## Page函数
小程序页面函数，每声明一个页面，就要调用Page
setData()

## tabbar

## 小程序页面跳转

1. navigateTo() 不能跳转到tab页，会产生页面栈，不会销毁上个页面，有返回按钮

2. redirectTo() 不能跳转到tab页，不会产生页面栈，会销毁上个页面，没有有返回按钮

3. switchTab() 跳转到tab页面

## 封装页面跳转方法

```js
 function({url = '', query = {}, redirect = false, tabbar = false}){
  // 处理query拼接url
  url = formatUrlQueryString(url, query);
  const methodName = tabbar ? 'switchTab' : redirect ? 'redirectTo' : 'navigateTo';
  // 如果跳转的页面是tabbar页面使用switchTab跳转
  // 如果redirect值为true使用redirectTo跳转
  // 以上两个条件都不符合使用navigateTo跳转并且解决十次的问题
  return new Promise((resolve, reject) => {
    uni[methodName]({
      url,
      success(res){
        resolve(res);
      },
      fail(error){
        reject(error)
      }
    })
  })
}
```

## 封装http请求方法

```js
import Vue from "vue";
import { formatUrlQueryString } from "./toPage"
// 请求方法，返回值是一个对象返回post, get 等请求方法
const httpTool = ({timeout, baseUrl, errorHandler, requestHandler}) => {
  // 发起uni.request
  const request = async (url, method = 'get', data = {}, header = {}) => {
    const requstObject = {
      url: `${baseUrl}${url}`,
      method,
      data,
      header,
      timeout,
    }; 
    // 判断请求前拦截 处理请求对象
    const config = requestHandler && requestHandler(requstObject) || requstObject;

    if(typeof config === 'object' && config.url && config.method){ // 请求对象是合法对对象
      try {
        const res = await uni.request(config);
        if(Array.isArray(res) && res.length && res[1]){
          return res[1].data ? res[1].data : res[1];
        }
        return Promise.reject(res);
      }catch(error){
        // 执行错误拦截
        errorHandler && errorHandler(error);
        return Promise.reject(error);
      }
    }
  }
  return {
    post(url, data, header = {}){
      return request(url, 'POST', data, header)
    },
    get(url, query, header = {}){
      // 处理url参数
      url = formatUrlQueryString(url, query);
      return request(url, 'GET', {}, header)
    } 
  }
}

const http = httpTool({
  timeout: 10000,
  baseUrl: 'https://bjwz.bwie.com/mall4j',
  requestHandler: (config) => { // 请求前拦截
    return {
      // 添加公共headers
      ...config,
      header: {
        'test':'test'
      }
    }
  },
  errorHandler: (error) => { // 错误拦截函数

  }
});

export default http;

Vue.prototype.$http = http;
```


### 小程序登录流程

1. `wx.login`获取code
2. 发送接口请求传递code
3. 得到请求结果，把token和其他用户信息，存入本地存储
4. 请求其他跟用户相关接口，从本地存储中取到用户token，携带制定header头

![登录](https://res.wx.qq.com/wxdoc/dist/assets/img/api-login.2fcc9f35.jpg)